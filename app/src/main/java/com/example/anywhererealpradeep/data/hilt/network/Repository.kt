package com.example.anywhererealpradeep.data.hilt.network

import android.util.Log
import com.example.anywhererealpradeep.data.hilt.SeriesApi
import com.example.anywhererealpradeep.models.SeriesData
import com.example.anywhererealpradeep.utils.Constants
import com.sample.simpsonsviewer.BuildConfig
import javax.inject.Inject


class Repository @Inject constructor(private val api: SeriesApi) {


    suspend fun getSeries(): DataOrException<SeriesData, Boolean, Exception> {
        return try {
            val response = when (BuildConfig.FLAVOR) {
                Constants.FLAVOR_SIMPSONS -> api.getSimpsonSeries()
                Constants.FLAVOR_WIRE -> api.getTheWire()
                else -> throw IllegalArgumentException("Invalid flavor: ")
            }
            Log.d("REPOSITORY_RESPONSE", "getSeries: $response")
            DataOrException(data = response)
        } catch (e: Exception) {
            Log.d("RESPONSE_ERROR", "getSeries: $e")
            DataOrException(e = e)
        }
    }


}