package com.example.anywhererealpradeep.data.hilt

import android.app.Application
import com.example.anywhererealpradeep.data.hilt.network.Repository
import com.example.anywhererealpradeep.utils.Constants
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideSeriesRepository(api: SeriesApi) = Repository(api)

    @Singleton
    @Provides
    fun provideSeriesApi(): SeriesApi {
        return Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(SeriesApi::class.java)
    }


}