package com.example.anywhererealpradeep.data.hilt

import com.example.anywhererealpradeep.models.SeriesData
import com.example.anywhererealpradeep.utils.Constants
import retrofit2.http.GET
import retrofit2.http.Query
import javax.inject.Singleton

@Singleton
interface SeriesApi {
    @GET(Constants.SIMPSONS_ENDPOINT)
    suspend fun getSimpsonSeries(): SeriesData

    @GET(Constants.WIRE_ENDPOINT)
    suspend fun getTheWire(): SeriesData
}