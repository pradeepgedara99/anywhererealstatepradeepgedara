package com.example.anywhererealpradeep.viewmodels

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.anywhererealpradeep.data.hilt.network.DataOrException
import com.example.anywhererealpradeep.data.hilt.network.Repository
import com.example.anywhererealpradeep.models.SeriesData
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(private val repository: Repository) : ViewModel() {

    private val _serisListLiveData =
        MutableLiveData<DataOrException<SeriesData, Boolean, Exception>>()
    val serisListLiveData: LiveData<DataOrException<SeriesData, Boolean, Exception>>
        get() = _serisListLiveData

    private val _name = MutableLiveData<String>()
    val name : LiveData<String>
        get() = _name


    init {
            getAllSeries()
    }

    private fun getAllSeries() {
        viewModelScope.launch {
            val result = repository.getSeries()
            Log.d("VIEWMODEL_RESPONSE", "getSeries: $result")
            _serisListLiveData.postValue(result)
        }
    }

    fun setResponse( name : String){
        _name.postValue(name)
    }


}