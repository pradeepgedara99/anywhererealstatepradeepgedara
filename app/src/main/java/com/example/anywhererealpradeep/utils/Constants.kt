package com.example.anywhererealpradeep.utils

object Constants {
    const val BASE_URL = "https://api.duckduckgo.com/"
    const val SIMPSONS_ENDPOINT = "?q=simpsons+characters&format=json"
    const val WIRE_ENDPOINT = "?q=the+wire+characters&format=json"

    // Flavor constants
    const val FLAVOR_SIMPSONS = "simpsons"
    const val FLAVOR_WIRE = "wire"
}