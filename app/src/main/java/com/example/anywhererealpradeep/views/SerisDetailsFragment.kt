package com.example.anywhererealpradeep.views

import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.example.anywhererealpradeep.viewmodels.MainViewModel
import com.sample.simpsonsviewer.R
import com.sample.simpsonsviewer.databinding.FragmentSerisDetailsBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SerisDetailsFragment : Fragment() {
    private lateinit var binding: FragmentSerisDetailsBinding
    private val args : SerisDetailsFragmentArgs by navArgs()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSerisDetailsBinding.inflate(inflater, container, false)
        observeData()
        Log.d("ARGUMENT_FROM_LIST", "onCreateView: ${args.relatedTopic.Icon.URL}")
        return binding.root
    }


    private fun observeData(){
        val imageURL = args.relatedTopic.FirstURL + args.relatedTopic.Icon.URL
                    binding.apply {
                        textViewResultDetails.text = args.relatedTopic.Result
                        textViewTextDetails.text = args.relatedTopic.Text
                        context?.let {
                            Glide.with(it)
                                .load(imageURL)
                                .override(300, 200)
                                .into(imageViewDetails)
                        }
                       // progressBarDetaiils.visibility = View.GONE
                    }

//                }
//                dataOrException.e != null -> {
//                    Toast.makeText(context,"Sorry Something wrong from our end", Toast.LENGTH_SHORT).show()
//                }
//            }
//        })

    }


}