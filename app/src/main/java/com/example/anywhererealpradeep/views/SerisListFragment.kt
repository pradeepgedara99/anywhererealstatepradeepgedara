package com.example.anywhererealpradeep.views

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.anywhererealpradeep.viewmodels.MainViewModel
import com.sample.simpsonsviewer.databinding.FragmentSerisListBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SerisListFragment : Fragment() {
    private val viewModel : MainViewModel by viewModels()
    private lateinit var binding : FragmentSerisListBinding
    private var adapter: SeriesAdapter? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSerisListBinding.inflate(inflater,container,false)
        adapter = SeriesAdapter(ArrayList(), requireContext(), onClick = { it ->
            Log.d("IMAGEURL_ARGS", "onCreateView: $it")
            val action = SerisListFragmentDirections.actionSerisListFragmentToSerisDetailsFragment(it)
            findNavController().navigate(action)
        })
        viewModel.setResponse("Wire")
        observeData(viewModel)
        return binding.root
    }

    private fun observeData(schoolViewModel: MainViewModel){
        schoolViewModel.serisListLiveData.observe(viewLifecycleOwner, Observer { dataOrException ->
            when {
                dataOrException.loading == true -> {
                    binding.progressBar.visibility = View.VISIBLE
                }
                dataOrException.data != null -> {
                    Log.d("RESPONSE_SERIES_DATA", "observeData: ${dataOrException.data}")
                    // Handle data state (e.g. update UI with data)
                    adapter!!.updateList(dataOrException.data!!.RelatedTopics!!)
                //    binding.tvShowResponse.text = dataOrException.data.toString()
                    binding.progressBar.visibility = View.GONE
                }
                dataOrException.e != null -> {
                    Toast.makeText(context,"Sorry Something wrong from our end when loading data",
                        Toast.LENGTH_SHORT).show()
                  //  binding.tvShowResponse.text = dataOrException.e.toString()
                }
            }
        })
        binding.recyclerview.layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerview.adapter = adapter

    }


}