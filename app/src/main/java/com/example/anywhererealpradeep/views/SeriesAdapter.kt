package com.example.anywhererealpradeep.views

import android.annotation.SuppressLint
import android.content.Context
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.anywhererealpradeep.models.RelatedTopic
import com.sample.simpsonsviewer.R
import com.sample.simpsonsviewer.databinding.ItemRelatedTopicBinding

class SeriesAdapter(dataModelList: List<RelatedTopic>, ctx: Context,
                           private val onClick :((dtoItem: RelatedTopic) -> Unit)? = null):
    RecyclerView.Adapter<SeriesAdapter.ViewHolder>(){
    private var dataModelList: List<RelatedTopic>
    private val context: Context

    init {
        this.dataModelList = dataModelList
        context = ctx
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ItemRelatedTopicBinding>(inflater,
            R.layout.item_related_topic, parent,false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val dataModel: RelatedTopic = dataModelList[position]
        bind(holder, dataModel)
    }

    private fun bind(holder: ViewHolder, dataModel: RelatedTopic) {
        holder.itemRowBinding.apply {
            textViewResult.text = dataModel.Result
            textFirstURLName.text = dataModel.FirstURL
            textViewText.text = dataModel.Text

            val iconURL = dataModel.FirstURL + dataModel.Icon.URL
            Log.d("IMAGE_URL_PICCASO", "bind: $iconURL")

            Glide.with(context)
                .load(iconURL)
                .override(300, 200)
                .into(textViewIcon)

            topicCardId.setOnClickListener {
                onClick?.let { it1 -> it1(dataModel) }
            }
        }

    }

    override fun getItemCount(): Int {
        return dataModelList.size
    }

    class ViewHolder(val itemRowBinding: ItemRelatedTopicBinding) :
        RecyclerView.ViewHolder(itemRowBinding.root) {


    }

    @SuppressLint("NotifyDataSetChanged")
    fun updateList(newList: List<RelatedTopic>) {
        dataModelList = newList
        notifyDataSetChanged()
    }



}