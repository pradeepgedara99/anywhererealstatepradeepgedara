package com.example.anywhererealpradeep.models

import java.io.Serializable

data class RelatedTopic(
    val FirstURL: String,
    val Icon: Icon,
    val Result: String,
    val Text: String
): Serializable