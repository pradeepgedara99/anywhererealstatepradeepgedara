package com.example.anywhererealpradeep.models

data class Maintainer(
    val github: String
)