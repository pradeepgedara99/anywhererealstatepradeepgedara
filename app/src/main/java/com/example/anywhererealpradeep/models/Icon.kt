package com.example.anywhererealpradeep.models

data class Icon(
    val Height: String,
    val URL: String,
    val Width: String
)