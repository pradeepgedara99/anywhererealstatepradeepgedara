package com.example.anywhererealpradeep.models

data class Developer(
    val name: String,
    val type: String,
    val url: String
)